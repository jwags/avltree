/*
 * avl_tree.c
 *
 * Copyright (C) 2016 Your Name Here
 */

#include <stdio.h>
#include <stdlib.h> /* needed for NULL */
#include <stdbool.h> /* needed for BOOL */

#include "avl_tree.h"

typedef struct avl_tree avl_tree;


struct avl_tree {
	avl_tree* parentAddress;
	avl_tree* leftChild;
	avl_tree* rightChild;
	uint64_t value;
	uint64_t height;
	//avl_tree* (*get_child)(const struct avl_tree*, int);
};

int what_child_am_i(avl_tree *t);
void balance_tree(avl_tree **t);
avl_tree *rotate_left(avl_tree *t);
avl_tree *rotate_right(avl_tree *t);
//avl_tree *get_child(avl_tree *t, int direction);
avl_tree *set_parents_child_to(avl_tree *t, int direction, avl_tree *value);
void printf_my_beutiful_avl_christmas_tree(avl_tree *t);



void print_tree(struct avl_tree* node);
int find_max_order_of_value_in_tree(struct avl_tree* node);
void execute_print_recursion(struct avl_tree* node, int MaxDepth, double size, int MaxOrder);
void finish_structure(struct avl_tree* node, int MaxDepth, double size, int MaxOrder);
int PowerOf(int num, int power);
void print_structure_line(struct avl_tree* node, int MaxDepth, int MaxOrder);
void print_value_line(struct avl_tree* node, int MaxDepth, int MaxOrder);
void execute_print_recursion_main(struct avl_tree* node, int MaxDepth, double size, int MaxOrder, bool hasNullLeftSibling, bool hasNullRightSibling);




int main(int argc, const char * argv[]) {
	//avl_tree *tree = NULL;
	avl_tree *tree1 = NULL;
	avl_tree *tree2 = NULL;
	avl_tree *tree3 = NULL;
	avl_tree *tree4 = NULL;
	//avl_tree *tree5 = NULL;
	//avl_tree *tree6 = NULL;
	avl_tree *tree7 = NULL;
	//tree = malloc(sizeof(avl_tree));
	tree1 = malloc(sizeof(avl_tree));
	tree2 = malloc(sizeof(avl_tree));
	tree3 = malloc(sizeof(avl_tree));
	tree4 = malloc(sizeof(avl_tree));
	//tree5 = malloc(sizeof(avl_tree));
	//tree6 = malloc(sizeof(avl_tree));
	tree7 = malloc(sizeof(avl_tree));
	//reallocate the trees
	//http://stackoverflow.com/questions/2329581/c-pointers-how-to-assign-value-to-a-pointer-struct
	
	
	//tree->value=0;
	//printf("hi\n");
	tree1->value = 1;
	tree1->height = 3;
	//printf("hi=%d\n",tree1->value);


	//tree6->value = 6;
	tree7->value = 7;
	tree2->value = 2;
	tree2->height = 2;
	tree3->value = 3;
	tree3->height = 2;
	tree4->value = 4;
	tree4->height = 1;
	//tree5->value = 5;
	//tree5->height = 1;
	//tree6->height = 1;
	tree7->height = 1;
	tree1->leftChild = tree2;
	//tree1->parentAddress = tree;
	//tree->leftChild = tree1;
	tree2->parentAddress = tree1;
	tree1->rightChild = tree3;
	tree3->parentAddress = tree1;
	//tree3->leftChild = tree6;
	tree3->rightChild = tree7;
	tree2->leftChild = tree4;
	//tree2->rightChild = tree5;
	tree4->parentAddress = tree2;
	//tree5->parentAddress = tree2;
	//tree6->parentAddress = tree3;
	tree7->parentAddress = tree3;







	avl_tree *tree8 = NULL;
	tree8 = malloc(sizeof(avl_tree));
	avl_tree *tree9 = NULL;
	tree9 = malloc(sizeof(avl_tree));
	//avl_tree *tree10 = NULL;
	//tree10 = malloc(sizeof(avl_tree));
	//avl_tree *tree11 = NULL;
	//tree11 = malloc(sizeof(avl_tree));
	avl_tree *tree12 = NULL;
	tree12 = malloc(sizeof(avl_tree));
	avl_tree *tree13 = NULL;
	tree13 = malloc(sizeof(avl_tree));
	avl_tree *tree14 = NULL;
	tree14 = malloc(sizeof(avl_tree));
	avl_tree *tree15 = NULL;
	tree15 = malloc(sizeof(avl_tree));
	tree8->height = 0;
	tree9->height = 0;
	//tree10->height = 0;
	//tree11->height = 0;
	tree12->height = 0;
	tree13->height = 0;
	tree14->height = 0;
	tree15->height = 0;
	tree8->value = 8;
	tree9->value = 9;
	//tree10->value = 10;
	//tree11->value = 11;
	tree12->value = 2;
	tree13->value = 3;
	tree14->value = 4;
	tree15->value = 5;


	tree4->leftChild = tree8;
	tree4->rightChild = tree9;
	
	//tree5->leftChild = tree10;
	//tree5->rightChild = tree11;
	
	//tree6->leftChild = tree12;
	//tree6->rightChild = tree13;
	
	tree7->leftChild = tree14;
	tree7->rightChild = tree15;


	tree8->parentAddress = tree4;
	tree9->parentAddress = tree4;
	//tree10->parentAddress = tree5;
	//tree11->parentAddress = tree5;
	//tree12->parentAddress = tree6;
	//tree13->parentAddress = tree6;
	tree14->parentAddress = tree7;
	tree15->parentAddress = tree7;
	
	printf("test\n");
	

	print_tree(tree1);
	printf("\n");
	tree1->value = 10;
	print_tree(tree1);
	printf("\n");
	tree1->value = 100;
	print_tree(tree1);
	printf("\n");
	tree1->value = 1000;
	print_tree(tree1);
	/*printf("Parent: %ju\n",tree1->value);
	printf("left: %ju\n",tree1->leftChild->value);
	printf("right: %ju\n",tree1->rightChild->value);
	printf("left-left: %ju\n",tree1->leftChild->leftChild->value);
	printf("left-right: %ju\n",tree1->leftChild->rightChild->value);
	//printf_my_beutiful_avl_christmas_tree(tree1);*/
	//tree1 = rotate_right(tree1);
	//printPaths(tree1);

	/*printf("\nsecond after rotate\n");
	printf("Parent: %ju\n",tree1->value);
	printf("left: %ju\n",tree1->leftChild->value);
	printf("right: %ju\n",tree1->rightChild->value);
	printf("right-left: %ju\n",tree1->rightChild->leftChild->value);
	printf("right-right: %ju\n",tree1->rightChild->rightChild->value);*/
	//tree1 = rotate_left(tree1);
	//printPaths(tree1);

	/*printf("\nthird after rotate\n");
	printf("Parent: %ju\n",tree1->value);
	printf("left: %ju\n",tree1->leftChild->value);
	printf("right: %ju\n",tree1->rightChild->value);
	printf("left-left: %ju\n",tree1->leftChild->leftChild->value);
	printf("left-right: %ju\n",tree1->leftChild->rightChild->value);*/
}

int PowerOf(int num, int power) {
	int total = num;
	int i = 0;
	if (power == 0) {
		return 1;
	}
	else if (power == 1 && num != 0) {
		return num;
	}
	else if (power == 1 && num == 0) {
		return 0;
	}
	for (i = 1; i<power; i++) {
		total = total*num;
	}
	return total;
}

//works for orders of 1 and 2. No so much 3+
void print_tree(struct avl_tree* node) {
	int i = 0;
	int MaxDepth = node->height;
	int maxOrder = find_max_order_of_value_in_tree(node);
	
	print_value_line(node, MaxDepth, maxOrder);
	printf("\n");
	print_structure_line(node, MaxDepth, maxOrder);
	printf("\n");
	
	MaxDepth = MaxDepth-1;
	for(i=0;i<=MaxDepth;i++) { 
		if(node->leftChild!=NULL)
			execute_print_recursion(node->leftChild, (MaxDepth-(i)), i, maxOrder);
		if(node->rightChild!=NULL)
			execute_print_recursion(node->rightChild, (MaxDepth-(i)), i, maxOrder);
		printf("\n");
		
		if((MaxDepth-i)>0) {
			if(node->leftChild!=NULL)
				finish_structure(node->leftChild, (MaxDepth-(i)), i, maxOrder);
			if(node->rightChild!=NULL)
				finish_structure(node->leftChild, (MaxDepth-(i)), i, maxOrder);
			printf("\n");
		}
	}
}
void execute_print_recursion(struct avl_tree* node, int MaxDepth, double size, int MaxOrder) {
	execute_print_recursion_main(node, MaxDepth, size, MaxOrder, false, false);
}
void execute_print_recursion_main(struct avl_tree* node, int MaxDepth, double size, int MaxOrder, bool hasNullLeftSibling, bool hasNullRightSibling) {
	int j, i;
	bool isNextRowNull = false;

	if((node->leftChild==NULL&&node->rightChild!=NULL)||hasNullLeftSibling) {
		//if(!hasNullSibling)
		//	heightOfNullSibling = node->height;
		if(hasNullLeftSibling) {
			//if(size==0) {
			if(node->height>0) {
				for(j=0;j<((PowerOf(2,((node->height)+1))*MaxOrder));j++) {
					printf(" ");
				}
			} else {
				if(MaxOrder>1) {
					for(j=0;j<MaxOrder;j++) {
						printf(" ");
					}
				}
			}
			//}
		}
		if(node->leftChild==NULL){
			hasNullLeftSibling = true;//printf("here%d",hasNullSibling);
		}
		else
			hasNullLeftSibling = false;
	}
	if((node->leftChild!=NULL&&node->rightChild==NULL))//||hasNullRightSibling) 
		isNextRowNull = true;

	if(node->leftChild!=NULL&&size>0) {
		if((size-1)==0 && MaxDepth==0)
			execute_print_recursion_main(node->leftChild,(-1),(size-1), MaxOrder, false, isNextRowNull);
		else
			execute_print_recursion_main(node->leftChild,(MaxDepth),(size-1), MaxOrder, false, isNextRowNull);
	}
	if(node->rightChild!=NULL&&size>0)
		execute_print_recursion_main(node->rightChild,(MaxDepth),(size-1), MaxOrder, hasNullLeftSibling, false);
	if(size==0) {
		print_value_line(node, MaxDepth, MaxOrder);
	}
	if(hasNullRightSibling) {//&&size>0) {
		if(node->height>0) {
			if(MaxOrder==1) {
				for(j=0;j<((PowerOf(2,((node->height)+1))*2));j++) {
					printf(" ");
				}
			} else {
				for(j=0;j<((PowerOf(2,((node->height)+1))*MaxOrder));j++) {
					printf(" ");
				}
			}
		} else {
			if(MaxOrder>1) {
				for(j=0;j<MaxOrder;j++) {
					printf(" ");
				}
			}
		}
	}
}

void print_value_line(struct avl_tree* node, int MaxDepth, int MaxOrder) {
	int structureSize;
	int j =0;
	int minTreeSize = (MaxOrder==1) ? 6 : MaxOrder*4;
	struct avl_tree *tempNode = NULL;
	
	if(MaxOrder==1)
		structureSize = (MaxDepth==0||MaxDepth==1 ? MaxDepth : (((((PowerOf(2,MaxDepth)/2)*minTreeSize)/2)-2)/2));
	else
		structureSize = (MaxDepth==0||MaxDepth==1 ? MaxDepth : ((((PowerOf(2,node->height)/4)*MaxOrder)+1) +((node->height-2)*MaxOrder)));
	for(j=0;(j<(structureSize-1))&&MaxDepth>1&&MaxOrder==1;j++) {
		printf(" ");
	}
	for(j=0;(j<structureSize)&&MaxDepth>1&&MaxOrder>1;j++) {
		printf(" ");
	}
	if(MaxOrder==1&&MaxDepth>1)
		printf(" ");
	if(MaxOrder==1&&MaxDepth>0)
		printf(" ");
	if(MaxOrder>1&&MaxDepth==1)
		printf(" ");
	if(MaxOrder>1&&MaxDepth==0) {
		for(j=0;j<MaxOrder;j++) {
			printf(" ");
		}
	}
	for(j=0;j<structureSize && MaxDepth!=-1&&MaxOrder==1;j++) {
		printf("_");
	}
	for(j=0;j<((((PowerOf(2,node->height)-2)/2)*MaxOrder)+(MaxOrder-1)) && MaxDepth!=-1&&MaxDepth!=0&&MaxOrder>1;j++) {
		printf("_");
	}
	printf("%.*d",MaxOrder,node->value);
	for(j=0;j<structureSize && MaxDepth!=-1&&MaxOrder==1;j++) {
		printf("_");
	}
	for(j=0;j<((((PowerOf(2,node->height)-2)/2)*MaxOrder)+(MaxOrder-1)) && MaxDepth!=-1&&MaxDepth!=0&&MaxOrder>1;j++) {
		printf("_");
	}
	if(MaxOrder>1&&MaxDepth>0) {
		for(j=0;(j<structureSize)&&MaxDepth>1&&MaxOrder>1;j++) {
			printf(" ");
		}
		if(MaxDepth==1)
			printf(" ");
		for(j=0;j<MaxOrder;j++) {
			printf(" ");
		}
	}
	if(MaxOrder>1&&MaxDepth==0) {
		for(j=0;j<MaxOrder;j++) {
			printf(" ");
		}
	}
	if(MaxOrder==1) {
		printf(" ");
		
		for(j=0;(j<structureSize)&&MaxDepth>1&&MaxOrder==1;j++) {
			printf(" ");
		}
		if(MaxDepth!=0)
			printf(" ");
		if(MaxDepth==-1)
			printf(" ");
	}
	
	/*if(node->parentAddress!=NULL && node->parentAddress->rightChild==NULL) {
		for(j==0;j<((PowerOf(2,node->height)+1)*MaxOrder);j++) {
			printf(" ");
		}
	}*/
}
//(PowerOf(2,((node->height)+1))*MaxOrder)
//((PowerOf(2,node->height)+1)*MaxOrder)

void finish_structure(struct avl_tree* node, int MaxDepth, double size, int MaxOrder) {
	int j;
	int minTreeSize = (MaxOrder==1) ? 4 : MaxOrder*4;
	int structureSize;
	if(node->leftChild!=NULL)
		finish_structure(node->leftChild,(MaxDepth),(size-1),MaxOrder);
	if(node->rightChild!=NULL)
		finish_structure(node->rightChild,(MaxDepth),(size-1),MaxOrder);
	if(node->leftChild!=NULL&&node->rightChild==NULL&&size>0) {//&&size>0) {
		if(node->height>0) {
			for(j=0;j<((PowerOf(2,((node->height)+1))*MaxOrder));j++) {
				printf(" ");
			}
			if(MaxOrder==1) {
				for(j=0;j<minTreeSize;j++) {
					printf(" ");
				}
			}
		} else {
			if(MaxOrder>1) {
				for(j=0;j<MaxOrder;j++) {
					printf(" ");
				}
			}
		}
	}
	if(size==0) {
		print_structure_line(node, MaxDepth, MaxOrder);
	}
}

void print_structure_line(struct avl_tree* node, int MaxDepth, int MaxOrder) {
	int structureSize;
	int j =0;
	int minTreeSize = (MaxOrder==1) ? 6 : MaxOrder*4;
	if(MaxOrder==1) {
		structureSize = (MaxDepth==0||MaxDepth==1 ? MaxDepth : (((((PowerOf(2,MaxDepth)/2)*minTreeSize)/2)-2)/2));
		for(j=0;(j<(structureSize-1))&&MaxDepth>1;j++) {
			printf(" ");
		}
		if(MaxOrder==1&&MaxDepth>1)
			printf(" ");
	} else {
		for(j=0;j<(((PowerOf(2,node->height)/2) - 1)*MaxOrder);j++) {
			printf(" ");
		}
	}
	printf("|");
	if(MaxOrder==1) {
		for(j=0;j<structureSize;j++) {
			printf(" ");
		}
		printf(" ");
		for(j=0;j<structureSize;j++) {
			printf(" ");
		}
	} else {
		for(j=0;j<(((PowerOf(2,((node->height)+1))-2)/2)*MaxOrder+((MaxOrder*2)-2));j++) {
			printf(" ");
		}
	}
	printf("|");
	if(MaxOrder==1) {
		for(j=0;(j<structureSize)&&MaxDepth>1;j++) {
			printf(" ");
		}
		printf(" ");
	} else {
		for(j=0;j<((((PowerOf(2,node->height)/2) - 1)*MaxOrder)+MaxOrder);j++) {
			printf(" ");
		}
	}
}

int find_max_order_of_value_in_tree(struct avl_tree* node) {
	int MaxOrder = 0;
	int NewOrderLeft = 0;
	int NewOrderRight = 0;
	int currentOrder = 0; 
	int currentValue = 0;
	if(node->leftChild!=NULL)
		NewOrderLeft = find_max_order_of_value_in_tree(node->leftChild);
	if(node->rightChild!=NULL)
		NewOrderRight = find_max_order_of_value_in_tree(node->rightChild);
	if(NewOrderLeft>MaxOrder)
		MaxOrder = NewOrderLeft;
	if(NewOrderRight>MaxOrder)
		MaxOrder = NewOrderRight;
	currentValue = node->value;
	while(currentValue != 0) {
		currentValue /= 10;
		++currentOrder;
	}
	return (MaxOrder > currentOrder ? MaxOrder : currentOrder);
}












avl_tree *avl_insert(avl_tree **t, uint64_t val) {

	return NULL;
}


avl_tree *avl_search(avl_tree *t, uint64_t val) {

	return NULL;
}


void avl_delete(avl_tree **t) {
	avl_tree *empty_tree = NULL;
	int child_direction = what_child_am_i(*t);
	set_parents_child_to(*t, child_direction, empty_tree);
}

void balance_tree(avl_tree **t) {

}

avl_tree *rotate_left(avl_tree *t) {
	int direction = what_child_am_i(t);
	bool isRoot = false;
	avl_tree *root = NULL;
	if(direction==0) {
		root = malloc(sizeof(avl_tree));
		isRoot = true;
		direction = 1;
		root->leftChild = t;
		t->parentAddress = root;
	}

	t = set_parents_child_to(t, direction, t->rightChild);
	t->rightChild->parentAddress = t->parentAddress;

	t->rightChild->leftChild->parentAddress = t;
	t->rightChild = t->rightChild->leftChild;

	//t->parentAddress->get_child(t->parentAddress,direction)->rightChild = t;
	if(direction==1) {
		t->parentAddress->leftChild->leftChild = t;
		t->parentAddress = t->parentAddress->leftChild;
	}
	else if(direction==2) {
		t->parentAddress->rightChild->leftChild = t;
		t->parentAddress = t->parentAddress->rightChild;
	}

	t = t->parentAddress;
	if(isRoot) {
		free(root);
		t->parentAddress = NULL;
	}
	return t;
}

avl_tree *rotate_right(avl_tree *t) {
	int direction = what_child_am_i(t);
	bool isRoot = false;
	avl_tree *root = NULL;
	if(direction==0) {
		root = malloc(sizeof(avl_tree));
		isRoot = true;
		direction = 1;
		root->leftChild = t;
		t->parentAddress = root;
	}

	t = set_parents_child_to(t, direction, t->leftChild);
	//t->parentAddress->leftChild = t->leftChild;
	t->leftChild->parentAddress = t->parentAddress;

	t->leftChild->rightChild->parentAddress = t;
	t->leftChild = t->leftChild->rightChild;

	//t->parentAddress->get_child(t->parentAddress,direction)->rightChild = t;
	if(direction==1) {
		t->parentAddress->leftChild->rightChild = t;
		t->parentAddress = t->parentAddress->leftChild;
	}
	else if(direction==2) {
		t->parentAddress->rightChild->rightChild = t;
		t->parentAddress = t->parentAddress->rightChild;
	}

	t = t->parentAddress;
	if(isRoot) {
		free(root);
		t->parentAddress = NULL;
	}
	return t;
}

//0=not child - 1=left child - 2=right child
int what_child_am_i(avl_tree *t) {
	if(t->parentAddress==NULL) {
		return 0;
	}
	if(t == t->parentAddress->leftChild) {
		return 1;
	} else if (t == t->parentAddress->rightChild) {
		return 2;
	} else {
		return 0;
	}
	return;
}

/*avl_tree* get_child(const struct avl_tree* t, int direction)
{
	return (direction==1?t->leftChild:t->rightChild);
}*/

avl_tree *set_parents_child_to(avl_tree *t, int direction, avl_tree *node) {
	if(direction==1) {
		t->parentAddress->leftChild = node;
	} else if(direction==2) {
		t->parentAddress->rightChild = node;
	} else {
		//not a child
	}
	return t;
}

//because I felt like it >:( Don't ask questions...
void printf_my_beutiful_avl_christmas_tree(avl_tree *t)
{
	if(t==NULL)
		return;
	printf("%d",t->value);
	if(t->leftChild!=NULL) {
		printf_my_beutiful_avl_christmas_tree(t->leftChild);
		
	}
	if(t->rightChild!=NULL) {
		printf_my_beutiful_avl_christmas_tree(t->rightChild);
	} else {

	}
}
//https://www.eskimo.com/~scs/cclass/int/sx8.html
//http://www.geeksforgeeks.org/avl-tree-set-1-insertion/
//http://stackoverflow.com/questions/506366/c-pointer-to-struct-in-the-struct-definition